function orderForm() {
  return {
    restrict: 'E', // A == attribute | E == element
    scope: {},
    bindToController: {
      data: '=',
      submit: '&'
    },
    controller: 'OrderFormController as form',
    template: `
      <pre>{{ orderForm | json }}</pre>
      <form name="orderForm" novalidate ng-submit="form.onSubmit()">
        <input name="name" required="" type="text" ng-model="form.data.name" placeholder="Your name"><br>
        <input name="email" type="email" ng-model="form.data.email" placeholder="Your email"><br>
        <input name="location" type="text" ng-model="form.data.location" placeholder="Your location"><br>
        <select>
          <option value="">Select...</option>
        </select><br>
        <textarea name="comments" ng-model="form.data.comments" placeholder="Any messages (optional)"></textarea>
        <button>Order</button>
      </form>
    `
  }
}

angular
  .module('app')
  .directive('orderForm', orderForm);
