function OrderController() {
  this.costumerOrder = {
    name: '',
    email: '',
    location: '',
    products: {
      label: '',
      id: ''
    },
    comments: ''
  };

  this.submitOrder = function() {
    // comunicate with API
  }
}

angular
  .module('app')
  .controller('OrderController', OrderController);
