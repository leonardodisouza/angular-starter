function UserService($http) {
    var API = '//jsonplaceholder.typicode.com/users/';

    // service approach
    // this.getUser = function(userId) {
    //   return $http.get(API + userId);
    // }
    //
    // this.getAllUsers = function() {
    //   return $http.get(API);
    // }

    // factory approach
    function getUser(userId) {
      return $http.get(API + userId);
    }

    function getAllUsers() {
      return $http.get(API);
    }

    return {
      getUser: getUser,
      getAllUsers: getAllUsers
    }
}

angular
  .module('app')
  .factory('UserService', UserService);
