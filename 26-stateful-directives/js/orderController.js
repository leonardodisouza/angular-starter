function OrderController() {
  this.orderName = 'Pepsi';
  this.orderCountry = 'BR';
  this.orderQuantity = 5;
}

angular
  .module('app')
  .controller('OrderController', OrderController);
