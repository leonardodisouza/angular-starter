function CounterController() {
  // console.log($scope.count);
  // this.count = $scope.count;
  // this.count = 1;

  this.decrement = function() {
    this.count -= 1;
  }

  this.increment = function() {
    this.count += 1;
  }
}

angular
  .module('app')
  .controller('CounterController', CounterController);
