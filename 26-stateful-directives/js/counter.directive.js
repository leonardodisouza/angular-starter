function counter() {
  return {
    restrict: 'E', // A == attribute | E == element
    controller: 'CounterController as counter',
    scope: {},
    bindToController: {
      title: '@name',
      region: '@country',
      count: '=' // '=count'
    },
    // controllerAs: 'counter',
    template: `
      <div class="counter">
        {{ counter.title }} - {{ counter.region }}
        <button type="button" ng-click="counter.decrement()"> - </button>
        <input type="text" ng-model="counter.count">
        <button type="button" ng-click="counter.increment()"> + </button>
      </div><hr>
    `
  }
}

angular
  .module('app')
  .directive('counter', counter);
