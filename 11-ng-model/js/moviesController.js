function MoviesController() {
  console.log(this);
  this.favorites = [
      { title: "The Shawshank Redemption", year: 1994, popular: true, category: 'action' }
    , { title: "Inception", year: 2010, popular: false, category: 'fantasy' }
    , { title: "The Matrix", year: 1999, popular: true, category: 'war' }
    , { title: "Saving Private Ryan", year: 1998, popular: true }
  ];
}

angular
  .module('app')
  .controller('MoviesController', MoviesController);
