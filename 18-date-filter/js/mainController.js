function MainController() {
  this.text = 'hello world!';

  this.obj = {
    key: 'value'
  };

  this.arr = ['my', 'angular', 'app'];

  this.num = 1500;

  this.date = new Date();
}

angular
  .module('app')
  .controller('MainController', MainController);
