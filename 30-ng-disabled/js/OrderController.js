function OrderController() {
  this.costumerOrder = {
    name: '',
    email: '',
    location: '',
    product: {
      label: '',
      id: ''
    },
    comments: ''
  };

  this.submitOrder = function() {
    // comunicate with API
    console.log('Submitted!', this.costumerOrder);
  }
}

angular
  .module('app')
  .controller('OrderController', OrderController);
