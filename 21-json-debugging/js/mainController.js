function MainController() {
  this.text = 'hello world!';

  this.obj = {
    key: 'value'
  };

  this.arr = ['my', 'angular', 'app'];

  this.num = 1500.23;

  this.date = new Date();

  this.person = {
    name: 'Leonardo Souza',
    age: 33,
    location: 'São Paulo'
  };
}

angular
  .module('app')
  .controller('MainController', MainController);
