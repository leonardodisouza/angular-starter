function MainController($s, $r) {
  console.log(arguments);
  $s.name="Leonardo";
}

//MainController.$inject = ['$scope'];

angular
  .module('app')
  .controller('MainController', ['$scope', '$rootScope', MainController]);
