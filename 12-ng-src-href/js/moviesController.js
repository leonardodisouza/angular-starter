function MoviesController() {
  console.log(this);
  this.favorites = [
      { imdb: 'tt0111161', title: "The Shawshank Redemption", year: 1994, popular: true, category: 'action', cover: 'MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_UX182_CR0,0,182,268_AL_.jpg' }
    , { imdb: 'tt1375666', title: "Inception", year: 2010, popular: false, category: 'fantasy', cover: 'MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg' }
    , { imdb: 'tt0133093', title: "The Matrix", year: 1999, popular: true, category: 'war', cover: 'MV5BMTkxNDYxOTA4M15BMl5BanBnXkFtZTgwNTk0NzQxMTE@._V1_UX182_CR0,0,182,268_AL_.jpg' }
    , { imdb: 'tt0120815', title: "Saving Private Ryan", year: 1998, popular: true, cover: 'MV5BNjczODkxNTAxN15BMl5BanBnXkFtZTcwMTcwNjUxMw@@._V1_UY268_CR7,0,182,268_AL_.jpg' }
  ];
}

angular
  .module('app')
  .controller('MoviesController', MoviesController);
