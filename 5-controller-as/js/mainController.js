function MainController() {
  console.log(this);
  this.name="Leonardo";
}

angular
  .module('app')
  .controller('MainController', MainController);
