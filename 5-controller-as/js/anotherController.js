function AnotherController() {
  console.log(this);
  this.name="Aparecido";
}

angular
  .module('app')
  .controller('AnotherController', AnotherController);
