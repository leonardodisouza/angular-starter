function orderForm() {
  return {
    restrict: 'E', // A == attribute | E == element
    scope: {},
    bindToController: {
      data: '=',
      submit: '&'
    },
    controller: 'OrderFormController as form',
    template: `
      <pre>{{ form.data | json }}</pre>
      <form name="orderForm" novalidate ng-submit="form.onSubmit()">
        <input name="name" required="" type="text" ng-model="form.data.name" placeholder="Your name"><br>
        <div ng-show="orderForm.name.$error.required && orderForm.name.$touched">Name is required!</div>

        <input name="email" type="email" ng-model="form.data.email" placeholder="Your email"><br>
        <div ng-show="orderForm.email.$error.required && orderForm.email.$touched">Email is required!</div>
        <div ng-show="orderForm.email.$error.email && orderForm.email.$touched">Email is invalid!</div>

        <input name="location" type="text" ng-model="form.data.location" placeholder="Your location"><br>

        <select name="orderChoice" required=""
          ng-model="form.data.product"
          ng-options="product.label for product in form.products">
          <option value="">Select...</option>
        </select><br>

        <textarea required="" name="comments" ng-model="form.data.comments" placeholder="Any messages (optional)"></textarea>
        <button style="background: cyan" ng-disabled="orderForm.$invalid">Order</button>
      </form>
    `
  }
}

angular
  .module('app')
  .directive('orderForm', orderForm);
