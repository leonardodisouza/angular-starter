function OrderFormController() {
  this.products = [
    { label: 'Product one', id: 1},
    { label: 'Product two', id: 2},
    { label: 'Product three', id: 3}
  ];

  this.onSubmit = function() {
    this.submit();
  }
}

angular
  .module('app')
  .controller('OrderFormController', OrderFormController);
