

function TodoController() {
  this.newTodo = '';

  this.list = [{
    title: 'First Todo Item!',
    completed: true
  },{
    title: 'Second Todo Item!',
    completed: false
  },{
    title: 'Third Todo Item!',
    completed: false
  },{
    title: 'Another Todo Item!',
    completed: false
  }];

  this.addTodo = function() {
    this.list.unshift({ title: this.newTodo, completed: false });
    this.newTodo = '';
  }

  this.removeTodo = function(item, index) {
    this.list.splice(index, 1);
  }

  this.getRemaining = function() {
    return this.list.filter(function(item) {
      return !item.completed;
    });
  }
}

angular
  .module('app')
  .controller('TodoController', TodoController);
