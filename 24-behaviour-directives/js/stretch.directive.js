function myStretch() {
  return {
    restrict: 'A', // A == attribute | E == element
    link: function($scope, $element, $attrs) {
      var elem = $element[0];

      console.log(elem);

      elem
        .addEventListener('focus', function() {
          this.style.width = '250px';
        });

      elem
        .addEventListener('blur', function() {
          this.style.width = '120px';
        });
    }
  }
}

angular
  .module('app')
  .directive('myStretch', myStretch);
