angular
  .module('app')
  .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $stateProvider
      .state('home', {
        url: '/',
        controller: 'HomeController as ctrl',
        template: `
          <p>{{ ctrl.viewName }}</p>
        `
      })
      .state('about', {
        url: '/about',
        controller: 'AboutController as ctrl',
        template: `
          <p>{{ ctrl.viewName }}</p>
        `
      });

    $urlRouterProvider.otherwise('/');
  });
