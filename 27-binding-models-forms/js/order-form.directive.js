function orderForm() {
  return {
    restrict: 'E', // A == attribute | E == element
    scope: {},
    bindToController: {
      data: '=',
      submit: '&'
    },
    controller: 'OrderFormController as form',
    template: `
      <pre>{{ form.data | json }}</pre>
      <form novalidate ng-submit="form.onSubmit()">
        <input type="text" ng-model="form.data.name" placeholder="Your name"><br>
        <input type="email" ng-model="form.data.email" placeholder="Your email"><br>
        <input type="text" ng-model="form.data.location" placeholder="Your location"><br>
        <select>
          <option value="">Select...</option>
        </select><br>
        <textarea ng-model="form.data.comments" placeholder="Any messages (optional)"></textarea>
        <button>Order</button>
      </form>
    `
  }
}

angular
  .module('app')
  .directive('orderForm', orderForm);
