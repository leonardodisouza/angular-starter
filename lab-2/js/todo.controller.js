function TodoController() {
  this.list = [{
    title: 'First Todo Item!',
    completed: true
  },{
    title: 'Second Todo Item!',
    completed: false
  },{
    title: 'Third Todo Item!',
    completed: false
  },{
    title: 'Another Todo Item!',
    completed: false
  }];
}

angular
  .module('app')
  .controller('TodoController', TodoController);
