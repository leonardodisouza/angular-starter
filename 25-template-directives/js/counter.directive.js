function counter() {
  return {
    restrict: 'E', // A == attribute | E == element
    controller: 'CounterController as counter',
    scope: {},
    // controllerAs: 'counter',
    template: `
      <div class="counter">
        <button type="button" ng-click="counter.decrement()"> - </button>
        <input type="text" ng-model="counter.count">
        <button type="button" ng-click="counter.increment()"> + </button>
      </div><hr>
    `
  }
}

angular
  .module('app')
  .directive('counter', counter);
